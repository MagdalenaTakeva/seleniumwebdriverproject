import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import java.util.List;


public class HomeworkSelenium1 {
    @Test
    public void navigateToBing(){
        System.setProperty("webdriver.chrome.driver", "C:\\Temp\\WebDrivers\\Chrome_driver\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        //navigate to Bing
        driver.get("https://www.bing.com/");
        WebElement query = driver.findElement(By.id("sb_form_q"));
        query.sendKeys("Telerik Academy Alpha" + Keys.ENTER);
        WebElement firstResult = driver.findElement(By.linkText("IT Career Start in 6 Months - Telerik Academy Alpha"));
        Assert.assertTrue("Search input is not displayed", firstResult.isDisplayed());
        //System.out.println(firstResult.getText());
        driver.quit();



    }
    //1st solution to GoogleChrome task
    @Test
    public void navigateToGoogleChrome(){
        System.setProperty("webdriver.chrome.driver", "C:\\Temp\\WebDrivers\\Chrome_driver\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();

        //Navigate to Google.com
        driver.manage().window().maximize();
        driver.get("https://www.google.com/");

        //Accept cookies
        Actions action = new Actions(driver);
        WebElement button = driver.findElement(By.id("L2AGLb"));
        action.moveToElement(button).click().perform();

        //Search for 'Telerik Academy Alpha'
        WebElement query = driver.findElement(By.name("q"));
        query.sendKeys("Telerik Academy Alpha", Keys.ENTER);

        //Validate the title of the first result as 'IT Career Start in 6 Months - Telerik Academy Alpha'
        List<WebElement> elementsList = driver.findElements(By.xpath("//h3[@class='LC20lb MBeuO DKV0Md']"));
        String expectedResult = "IT Career Start in 6 Months - Telerik Academy Alpha";
        Assert.assertEquals(expectedResult, elementsList.get(0).getText());
        driver.quit();

    }

    //2nd solution to GoogleChrome task
    @Test
    public void navigateToGoogleChrome2(){
        System.setProperty("webdriver.chrome.driver", "C:\\Temp\\WebDrivers\\Chrome_driver\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        //navigate to Google.com
        driver.get("https://www.google.com/");
        //Accept or reject cookies
        driver.findElement(By.id("L2AGLb")).click();

        //Search for 'Telerik Academy Alpha'
        driver.findElement(By.xpath("//input[@name='q']")).sendKeys("Telerik Academy Alpha", Keys.ENTER);

        //Validate the title of the first result as 'IT Career Start in 6 Months - Telerik Academy Alpha'
        //use //h3[@class='LC20lb MBeuO DKV0Md'] - returns H3 element with class="LC20lb MBeuO DKV0Md"
        List<WebElement> elementsList = driver.findElements(By.xpath("//h3[@class='LC20lb MBeuO DKV0Md']"));
        String expectedResult = "IT Career Start in 6 Months - Telerik Academy Alpha";
        Assert.assertEquals(expectedResult, elementsList.get(0).getText());
        driver.quit();



    }

    //3rd solution to GoogleChrome task
    @Test
    public void navigateToGoogleChrome4(){
        System.setProperty("webdriver.chrome.driver", "C:\\Temp\\WebDrivers\\Chrome_driver\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        //navigate to Google.com
        driver.manage().window().maximize();
        driver.get("https://www.google.com/");

        //Accept cookies
        driver.findElement(By.id("L2AGLb")).click();

        //Search for 'Telerik Academy Alpha'
        driver.findElement(By.name("q")).sendKeys("Telerik Academy Alpha", Keys.ENTER);

        //Validate the title of the first result as 'IT Career Start in 6 Months - Telerik Academy Alpha'
        //Use this xpath: //*[.="IT Career Start in 6 Months - Telerik Academy Alpha"]
        // - returns element containing text 'IT Career Start in 6 Months - Telerik Academy Alpha' exactly
        List<WebElement> elementsList = driver.findElements(By.xpath("//*[.='IT Career Start in 6 Months - Telerik Academy Alpha']"));
        String expectedResult = "IT Career Start in 6 Months - Telerik Academy Alpha";
        Assert.assertEquals(expectedResult, elementsList.get(0).getText());
        driver.quit();

    }

    //4th solution to GoogleChrome task
    @Test
    public void navigateToGoogleChrome5(){
        System.setProperty("webdriver.chrome.driver", "C:\\Temp\\WebDrivers\\Chrome_driver\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        //navigate to Google.com
        driver.manage().window().maximize();
        driver.get("https://www.google.com/");

        //Accept cookies
        driver.findElement(By.id("L2AGLb")).click();

        //Search for 'Telerik Academy Alpha'
        driver.findElement(By.name("q")).sendKeys("Telerik Academy Alpha", Keys.ENTER);

        //Validate the title of the first result as 'IT Career Start in 6 Months - Telerik Academy Alpha'
        //Use this xpath template /div[@role="main"]//h3 - returns first h3 of a div element with role="main"
        List<WebElement> elementsList = driver.findElements(By.xpath("//div[@role=\"main\"]//h3[1]"));
        String expectedResult = "IT Career Start in 6 Months - Telerik Academy Alpha";
        Assert.assertEquals(expectedResult, elementsList.get(0).getText());
        driver.quit();

    }



    //5th solution to GoogleChrome task
    @Test
    public void navigateToGoogleChrome6(){
        System.setProperty("webdriver.chrome.driver", "C:\\Temp\\WebDrivers\\Chrome_driver\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        //navigate to Google.com
        driver.manage().window().maximize();
        driver.get("https://www.google.com/");

        //Accept cookies
        driver.findElement(By.id("L2AGLb")).click();

        //Search for 'Telerik Academy Alpha'
        WebElement query = driver.findElement(By.name("q"));
        query.sendKeys("Telerik Academy Alpha", Keys.ENTER);

        //Validate the title of the first result as 'IT Career Start in 6 Months - Telerik Academy Alpha'
        //Use this xpath template //a[@href="https://www.telerikacademy.com/alpha"]//h3
        // - returns first h3 of a element with href="https://www.telerikacademy.com/alpha"
        List<WebElement> elementsList = driver.findElements(By.xpath("//a[@href=\"https://www.telerikacademy.com/alpha\"]//h3"));
        String expectedResult = "IT Career Start in 6 Months - Telerik Academy Alpha";
        Assert.assertEquals(expectedResult, elementsList.get(0).getText());
        driver.quit();

    }
}
